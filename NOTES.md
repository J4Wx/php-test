# Jamie Wood's Pokedex

Some parts of this were entirely unnecessary. I built the container/dependency injection/routing because I wanted to give it a try, it's not as complicated as I expected (though, this is a very rudimentary implementation).

This uses Redis for two reasons:
1. Rate Limiting
2. Speed

The search logic isn't the nicest, but it does the job without utilising something like elastic search and is still fairly fast, especially with Redis.

I have included a docker-compose to run App and Redis. If you want to run the app without docker then you'll need to update the redis host in .env.

To use the Twig ViewCache you'll need to create a folder: src/App/ViewCache and ensure it has sufficient permissions for docker to write to it. (777 will work, but I don't recommend it in Prod!)

Frontend isn't my strong point and my design ability is... worse, so Bootstrap it is!

I'd like to have written some tests, but I'm already pushing 3 and a half hours. I got carried away with IoC!

The docker-compose binds the Apache port 80 to the hosts port 8080.

http://localhost:8080
