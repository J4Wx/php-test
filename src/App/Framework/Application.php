<?php

namespace App\Framework;

use App\Controllers\SearchController;
use App\Framework\ReqRes\Request;
use App\Framework\ReqRes\Response;
use App\Framework\ReqRes\Router;
use App\Framework\Utilities\DependencyInjector;
use App\Repositories\PokeAPI\Cache\Utility;
use App\Repositories\PokeAPI\Client;

class Application
{
    public Request $request;
    public Response $response;
    public DependencyInjector $dependencyInjector;
    public string $baseDir;

    public function __construct()
    {
        $this->baseDir = __DIR__ . '/../../';

        $GLOBALS['application'] = $this;

        $this->request = Request::initialise();
        $this->response = Response::initialise();
        
        $this->dependencyInjector = new DependencyInjector();
        
        $this->dependencyInjector->bind(Request::class, $this->request);
        $this->dependencyInjector->bind('request', $this->request);

        $this->dependencyInjector->bind(Response::class, $this->response);
        $this->dependencyInjector->bind('response', $this->response);

        $this->dependencyInjector->bind(Utility::class, new Utility());

        $this->dependencyInjector->bind(Client::class, $this->dependencyInjector->make(Client::class));

        app()->dependencyInjector->make(SearchController::class);
    }
    
    public function handle()
    {
        $router = $this->dependencyInjector->make(Router::class);
        $router->resolve();
        $router->handle();
    }
}
