<?php

namespace App\Framework\Utilities;

use ReflectionClass;

class DependencyInjector
{
    private $bindings = [];

    public function bind(string $classname, $concrete)
    {
        $this->bindings[$classname] = $concrete;
    }

    public function load(string $classname)
    {
        return $this->bindings[$classname];
    }

    public function make(string $classname)
    {
        $class = new ReflectionClass($classname);
        $constructorParams = $class->getConstructor()->getParameters();

        $params = [];
        foreach ($constructorParams as $param) {
            $params[] = $this->load($param->getClass()->name);
        }

        return new $classname(...$params);
    }
}
