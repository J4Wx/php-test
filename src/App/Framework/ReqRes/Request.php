<?php

namespace App\Framework\ReqRes;

class Request
{
    private string $host;
    private string $userAgent;
    private string $accept;
    private string $scheme;
    private array $params;
    private string $path;
    private string $fullUrl;

    public static function initialise(): Request
    {
        $req = new self;
        $req->setHost($_SERVER['HTTP_HOST']);
        $req->setUserAgent($_SERVER['HTTP_USER_AGENT']);
        $req->setAccept($_SERVER['HTTP_ACCEPT']);
        $req->setScheme($_SERVER['REQUEST_SCHEME']);
        $req->setParams($_REQUEST);
        $req->setPath(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
        $req->setFullUrl($_SERVER['REQUEST_URI']);
        $req->setMethod($_SERVER['REQUEST_METHOD']);

        return $req;
    }

    public function setHost($host)
    {
        $this->host = $host;
    }

    public function host()
    {
        return $this->host;
    }

    public function setUserAgent($agent)
    {
        $this->userAgent = $agent;
    }

    public function userAgent()
    {
        return $this->userAgent;
    }

    public function setAccept($accept)
    {
        $this->accept = $accept;
    }

    public function accept()
    {
        return $this->accept;
    }

    public function setScheme($scheme)
    {
        $this->scheme = $scheme;
    }

    public function scheme()
    {
        return $this->scheme;
    }
    
    public function setParams($params)
    {
        $this->params = $params;
    }

    public function params(string $param = null)
    {
        if ($param) {
            return $this->params[$param] ?? null;
        }

        return $this->params;
    }

    public function setPath($path)
    {
        $this->path = $path;
    }

    public function path()
    {
        return $this->path;
    }

    public function setMethod($method)
    {
        $this->method = $method;
    }

    public function method()
    {
        return $this->method;
    }

    public function setFullUrl($url)
    {
        $this->fullUrl = $url;
    }

    public function getFullUrl()
    {
        return $this->fullUrl;
    }
}
