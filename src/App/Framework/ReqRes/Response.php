<?php

namespace App\Framework\ReqRes;

use Twig\Environment;
use Twig\Extension\DebugExtension;
use Twig\Loader\FilesystemLoader;

class Response
{
    public Environment $twig;

    public function __construct()
    {
        $loader = new FilesystemLoader(app()->baseDir . 'App/Views');
        $twigConfig = [];

        if (getenv('TWIG_CACHE') == 'true') {
            $twigConfig['cache'] = app()->baseDir . 'App/ViewCache';
        }

        if (getenv('DEBUG') == 'true') {
            $twigConfig['debug'] = true;
        }

        $this->twig = new Environment($loader, $twigConfig);

        if (getenv('DEBUG') == 'true') {
            $this->twig->addExtension(new DebugExtension());
        }
    }

    public static function initialise(): Response
    {
        return new self;
    }

    public function view(string $view, array $params)
    {
        echo $this->twig->render($view . '.html.twig', $params);
    }
}
