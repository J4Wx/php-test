<?php

namespace App\Framework\ReqRes;

use Exception;

class Router
{
    private string $controllerNamespace = 'App\\Controllers\\';

    private Request $request;
    private $routes = [];

    private string $controller;
    private string $action;


    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->routes['GET'] = [];

        $GLOBALS['router'] = $this;
        require_once __DIR__ . '/../../../router.php';
    }

    private function formatRoute($route): string
    {
        $result = rtrim($route, '/');
        
        if ($result === '') {
            $result = '/';
        }

        $result = str_replace($result, '\/', '/');

        return $result;
    }

    private function invalidMethodHandler()
    {
        header("405 Method Not Allowed");
    }

    private function defaultRequestHandler()
    {
        header("404 Not Found");
    }

    public function get(string $route, string $controllerMethod)
    {
        $this->routes['GET'][$this->formatRoute($route)] = $controllerMethod;
    }

    public function resolve()
    {
        $path = $this->request->path();
        $method = $this->request->method();

        foreach ($this->routes[$method] as $route => $action) {
            $match = preg_match('/^' . $route . '/', $path);

            if ($match) {
                $exAction = explode('@', $action);
                if (count($exAction) !== 2) {
                    throw new Exception('Invalid Router Action: ' . $action);
                }

                $this->controller = $exAction[0];
                $this->action = $exAction[1];
                break;
            }
        }
    }

    public function handle()
    {
        if ($this->controller) {
            $controller = app()->dependencyInjector->make($this->controllerNamespace . $this->controller);
            $action = $this->action;
            return $controller->$action();
        }

        return $this->defaultRequestHandler();
    }
}
