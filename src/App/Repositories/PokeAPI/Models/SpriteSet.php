<?php

namespace App\Repositories\PokeAPI\Models;

class SpriteSet
{
    public ?string $back;
    public ?string $backShiny;
    public ?string $front;
    public ?string $frontShiny;

    public static function fromArray(array $array, $gender): SpriteSet
    {
        $sprites = new self();
        
        if ($gender == Pokemon::MALE) {
            $sprites->back = $array['back_default'];
            $sprites->backShiny = $array['back_shiny'];
            $sprites->front = $array['front_default'];
            $sprites->frontShiny = $array['front_shiny'];
        } else {
            $sprites->back = $array['back_female'];
            $sprites->backShiny = $array['back_shiny_female'];
            $sprites->front = $array['front_female'];
            $sprites->frontShiny = $array['front_shiny_female'];
        }

        return $sprites;
    }

    public static function __set_state($properties)
    {
        $sprites = new self();
        $sprites->back = $properties['back'];
        $sprites->backShiny = $properties['backShiny'];
        $sprites->front = $properties['front'];
        $sprites->frontShiny = $properties['frontShiny'];
        return $sprites;
    }
}
