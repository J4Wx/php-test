<?php

namespace App\Repositories\PokeAPI\Models;

class Species
{
    public string $name;
    public string $url;

    public static function fromArray(array $array): Species
    {
        $species = new self();
        $species->name = $array['name'];
        $species->url = $array['url'];

        return $species;
    }

    public static function __set_state($properties)
    {
        $s = new self();
        $s->name = $properties['name'];
        $s->url = $properties['url'];
        
        return $s;
    }
}
