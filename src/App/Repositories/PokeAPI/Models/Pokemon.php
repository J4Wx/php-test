<?php

namespace App\Repositories\PokeAPI\Models;

class Pokemon
{
    public string $name;
    public int $id;
    public int $height;
    public int $weight;
    public Species $species;
    public SpriteSet $maleSprites;
    public SpriteSet $femaleSprites;

    public array $abilities;

    const MALE = 'default';
    const FEMALE = 'female';

    public static function fromArray(array $array): Pokemon
    {
        $pokemon = new self;
        $pokemon->id = $array['id'];
        $pokemon->name = $array['name'];
        $pokemon->height = $array['height'];
        $pokemon->weight = $array['weight'];
        $pokemon->species = Species::fromArray($array['species']);

        $pokemon->maleSprites = SpriteSet::fromArray($array['sprites'], self::MALE);
        $pokemon->femaleSprites = SpriteSet::fromArray($array['sprites'], self::FEMALE);

        foreach ($array['abilities'] as $ability) {
            $pokemon->abilities[] = Ability::fromArray($ability);
        }

        return $pokemon;
    }

    public static function __set_state($properties)
    {
        $pokemon = new self;
        $pokemon->id = $properties['id'];
        $pokemon->name = $properties['name'];
        $pokemon->height = $properties['height'];
        $pokemon->weight = $properties['weight'];
        $pokemon->species = $properties['species'];

        $pokemon->maleSprites = $properties['maleSprites'];
        $pokemon->femaleSprites = $properties['femaleSprites'];

        $pokemon->abilities = $properties['abilities'];

        return $pokemon;
    }
}
