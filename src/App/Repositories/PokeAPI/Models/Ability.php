<?php

namespace App\Repositories\PokeAPI\Models;

class Ability
{
    public string $name;
    public string $url;

    public static function fromArray(array $array): Ability
    {
        $ability = new self();
        $ability->name = $array['ability']['name'];
        $ability->url = $array['ability']['url'];
        
        return $ability;
    }

    public static function __set_state($properties)
    {
        $a = new self();
        $a->name = $properties['name'];
        $a->url = $properties['url'];
        return $a;
    }
}
