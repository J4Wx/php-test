<?php

namespace App\Repositories\PokeAPI;

use App\Framework\ReqRes\Request;
use App\Repositories\PokeAPI\Cache\Utility;
use App\Repositories\PokeAPI\Models\Pokemon;
use GuzzleHttp\Client as Guzzle;
use GuzzleHttp\Exception\ClientException;

class Client
{
    private Guzzle $http;
    private Request $request;
    private Utility $cache;

    public function __construct(Request $request, Utility $cache)
    {
        $this->request = $request;
        $this->cache = $cache;

        $this->http = new Guzzle([
            'base_uri' => 'http://pokeapi.co/api/v2/',
        ]);
    }

    public function getPokemon($nameOrId)
    {
        $nameOrId = strtolower($nameOrId);

        $cached = null;
        $limit = 20;
        $page = intval($this->request->params('page')) ?? 1;

        if ($page < 1) {
            $page = 1;
        }

        $offset = $limit * ($page - 1);

        if (!is_null($nameOrId)) {
            $cached = $this->cache->get($nameOrId . "?offset=$offset");
        }

        if (empty($nameOrId)) {
            return $this->search($nameOrId, $offset, $page);
        }

        if (!is_null($cached)) {
            return $cached;
        }

        $query = [
            'limit' => $limit
        ];

        if ($offset > 0) {
            $query['offset'] = $offset;
        }

        try {
            $resp = $this->http->get("pokemon/$nameOrId", [
                'query' => $query
            ]);
        } catch (ClientException $e) {
            $pokemon = $this->search($nameOrId, $offset, $page);
            return $pokemon;
        }

        if ($resp->getStatusCode() >= 400) {
            dd($resp->getReasonPhrase());
        }

        $body = $resp->getBody()->getContents();
        $body = json_decode($body, true);

        if (!isset($body['count'])) {
            $pokemon = Pokemon::fromArray($body);
            $this->cache->set($nameOrId, $pokemon);

            return $pokemon;
        }
    }

    public function search($nameOrId, $offset, $page)
    {
        $pokemon = $this->cache->get('ALL_POKEMON');

        if (is_null($pokemon)) {
            $resp = $this->http->get("pokemon", [
                'query' => [
                    'limit' => 9999
                ]
            ]);
            
            $pokemon = [];

            $resp = $resp->getBody()->getContents();
            $resp = json_decode($resp, true)['results'];

            foreach ($resp as $item) {
                $url = explode('/', $item['url']);
                array_pop($url);
                $pokemon[ucwords($item['name'])] = array_pop($url);
            }

            $this->cache->set('ALL_POKEMON', $pokemon);
        }

        // This isn't the nicest thing to do, but it saves me from using a proper search utility...
        if (!empty($nameOrId)) {
            uksort($pokemon, function ($a, $b) use ($nameOrId) {
                similar_text($nameOrId, $a, $resA);
                similar_text($nameOrId, $b, $resB);
                
                return $resA === $resB ? 0 : ($resA > $resB ? -1 : 1);
            });
        }

        $count = count($pokemon);

        $return = array_splice($pokemon, $offset, 20);

        $return = [
            'pokemon' => $return,
            'page' => $page,
            'max_page' => (int)ceil($count / 20)
        ];

        $this->cache->set($nameOrId . "?offset=$offset", $return);

        return $return;
    }
}
