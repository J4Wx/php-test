<?php

namespace App\Repositories\PokeAPI\Cache;

use Predis\Client;

class Utility
{
    public Client $client;

    public function __construct()
    {
        $this->client = new Client([
            'host' => getenv('REDIS_HOST')
        ]);
    }

    public function set(string $key, $data)
    {
        $data = var_export($data, true);
        return $this->client->set($key, $data);
    }

    public function get(string $key)
    {
        $cache = $this->client->get($key);

        if (is_null($cache)) {
            return null;
        }

        eval('$cache = ' . $cache . ';');

        return null;
        return $cache;
    }
}
