<?php

namespace App\Controllers;

use App\Framework\ReqRes\Request;
use App\Framework\ReqRes\Response;
use App\Repositories\PokeAPI\Client;

class SearchController
{
    private Request $request;
    private Response $response;
    private Client $client;

    public function __construct(Request $request, Response $response, Client $client)
    {
        $this->request = $request;
        $this->response = $response;
        $this->client = $client;
    }

    public function index()
    {
        $response = $this->client->getPokemon($this->request->params('search'));
        if (gettype($response) == "object") {
            $this->response->view('pokemon', ['pokemon' => $response]);
        } else {
            $page = intval($this->request->params('page')) ?? 1;

            if ($page < 1) {
                $page = 1;
            }

            $url = parse_url($this->request->getFullUrl());
            if (isset($url['query'])) {
                parse_str($url['query'], $params);
            } else {
                $params = [];
            }

            $nextPageParams = $params;
            $nextPageParams['page'] = $page + 1;
            $nextPageQuery = http_build_query($nextPageParams);

            $prevPageParams = $params;
            $prevPageParams['page'] = $page - 1;
            $prevPageQuery = http_build_query($prevPageParams);

            $response['nextPageUrl'] = $this->request->path() . "?$nextPageQuery";
            $response['prevPageUrl'] = $this->request->path() . "?$prevPageQuery";

            $this->response->view('index', $response);
        }
    }
}
