<?php

use App\Framework\Application;

function app(): Application
{
    return $GLOBALS['application'];
}

function dd($object)
{
    echo '<pre>';
    var_dump($object);
    die;
}
